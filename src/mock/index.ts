import { LocalGithubUser } from "../types";

export const defaultUser: LocalGithubUser = {
  login: "gaearon",
  avatar: "https://avatars.githubusercontent.com/u/810438?v=4",
  name: "dan",
  company: "@bluesky-social ",
  blog: "",
  location: null,
  bio: null,
  twitter: "dan_abramov",
  repos: 268,
  followers: 84986,
  following: 172,
  created: "2011-05-25T18:18:31Z",
};
