import { useState } from "react";
import { GithubError, GithubUser, LocalGithubUser } from "./types";
import { isGithubUser } from "./utils/typeguards";
import { Container } from "components/Container";
import { TheHeader } from "components/TheHeader";
import { Search } from "components/Search";
import { UserCard } from "components/UserCard";
import { defaultUser } from "./mock";
import { extractLocalUser } from "./utils/extract-local-user";

const BASE_URL = "https://api.github.com/users/";

function App() {
  const [githubUser, setGithubUser] = useState<LocalGithubUser | null>(
    defaultUser
  );

  const fetchUser = async (username: string) => {
    const fetchUrl = BASE_URL + username;
    const response = await fetch(fetchUrl);
    const user = (await response.json()) as GithubUser | GithubError;

    if (isGithubUser(user)) {
      setGithubUser(extractLocalUser(user));
    } else {
      setGithubUser(null);
    }
  };

  return (
    <Container>
      <TheHeader />
      <Search hasError={!githubUser} onSubmit={fetchUser} />
      {githubUser && <UserCard {...githubUser} />}
    </Container>
  );
}

export default App;
