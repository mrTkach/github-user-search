import { LocalGithubUser } from "types";
import { InfoItem, InfoItemProps } from "components/InfoItem";
import { ReactComponent as IconCompany } from "assets/icon-company.svg";
import { ReactComponent as IconLocation } from "assets/icon-location.svg";
import { ReactComponent as IconTwitter } from "assets/icon-twitter.svg";
import { ReactComponent as IconWebsite } from "assets/icon-website.svg";
import styles from "./UserInfo.module.scss";

interface UserInfoProps
  extends Pick<LocalGithubUser, "blog" | "company" | "location" | "twitter"> {}

export const UserInfo = ({
  blog,
  company,
  location,
  twitter,
}: UserInfoProps) => {
  const items: InfoItemProps[] = [
    {
      icon: <IconLocation />,
      text: location,
    },
    {
      icon: <IconCompany />,
      text: company,
    },
    {
      icon: <IconTwitter />,
      text: twitter,
    },
    {
      icon: <IconWebsite />,
      text: blog,
      isLink: true,
    },
  ];
  return (
    <div className={styles.userInfo}>
      {items.map((el, index) => (
        <InfoItem key={index} {...el} />
      ))}
    </div>
  );
};
