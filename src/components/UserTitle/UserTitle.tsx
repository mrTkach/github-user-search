import styles from "./UserTitle.module.scss";
import { LocalGithubUser } from "../../types";

interface UserTitleProps
  extends Pick<LocalGithubUser, "name" | "login" | "created"> {}

const BASE_GITHUB_URL = "https://github.com/";

const localDate = new Intl.DateTimeFormat("en-GB", {
  day: "numeric",
  month: "short",
  year: "numeric",
});

export const UserTitle = ({ name, login, created }: UserTitleProps) => {
  const joinedDate = localDate.format(new Date(created));

  return (
    <div className={styles.userTitle}>
      <h2>{name}</h2>
      <h3>
        <a
          href={BASE_GITHUB_URL + login}
          target="_blank"
          rel="noreferrer"
          className={styles.loginLink}
        >
          {login}
        </a>
      </h3>
      <span>{joinedDate}</span>
    </div>
  );
};
